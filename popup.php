<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>KEELA | Client Testimonials </title>

        <?php include('includes/header_assets.php'); ?>

    </head>

    <body>
        <div class="container px-5 py-5">
            <a href="#" class="btn black" data-toggle="modal" data-target="#loginModal">Log In <span class="arrow"></span> </a>
            <a href="#" class="btn black" data-toggle="modal" data-target="#signupModal">Sign Up <span class="arrow"></span> </a>
            <a href="#" class="btn black" data-toggle="modal" data-target="#requestinformation">Request Information<span class="arrow"></span> </a>
            <a href="#" class="btn black" data-toggle="modal" data-target="#schedule">Schedule Showing<span class="arrow"></span> </a>
        </div>


        <!-- Sign IN -->
        <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
            <div class="modal-dialog custom-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Log in to your account</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label class="text-uppercase"><strong>Email</strong></label>
                                <input type="email" name="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="text-uppercase"><strong>Password</strong></label>
                                <input type="password" name="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox remember_me">
                                    <input type="checkbox" class="custom-control-input" id="checkbox1">
                                    <label class="custom-control-label" for="checkbox1"> Remember me </label>
                                </div>
                            </div>
                            <button type="submit" class="btn black">Login <span class="arrow"></span> </button>
                        </form>
                    </div>
                    <div class="modal-footer justify-content-start">
                        <div class="signup_note">Do you want to <a href="#">Sign Up </a> for new Account or have you <a href="#">forgotten your password?</a></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Signup Popup -->
        <div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="signupModal" aria-hidden="true">
            <div class="modal-dialog custom-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Sign up for new account</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">                                        
                                        <label class="text-uppercase"><strong>First Name <span class="required_field">*</span></strong></label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="text-uppercase"><strong>Last Name <span class="required_field">*</span></strong></label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                            </div> <!-- row -->
                            <div class="form-group">
                                <label class="text-uppercase"><strong>Email <span class="required_field">*</span></strong></label>
                                <input type="email" name="email" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label class="text-uppercase"><strong>Password <span class="required_field">*</span></strong></label>
                                <input type="password" name="password" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label class="text-uppercase"><strong>Confirm Password <span class="required_field">*</span></strong></label>
                                <input type="password" name="password" class="form-control" required>
                            </div>
                            <div class="mt-3">
                                <button type="submit" class="btn black">Sign Up <span class="arrow"></span> </button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer justify-content-start">
                        <div class="signup_note">Already member?  <a href="#">Login </a></div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Requestin Information -->
        <div class="modal fade" id="requestinformation" tabindex="-1" role="dialog" aria-labelledby="requestinformation" aria-hidden="true">
            <div class="modal-dialog custom-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Request Information</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">                                        
                                        <label class="text-uppercase"><strong>First Name <span class="required_field">*</span></strong></label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="text-uppercase"><strong>Last Name <span class="required_field">*</span></strong></label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                            </div> <!-- row -->
                            <div class="form-group">
                                <label class="text-uppercase"><strong>Email <span class="required_field">*</span></strong></label>
                                <input type="email" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label class="text-uppercase"><strong>Phone <span class="required_field">*</span></strong></label>
                                <input type="tel" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label class="text-uppercase"><strong>Message</strong></label>
                                <textarea class="form-control"> </textarea>
                            </div>
                            <div class="mt-3">
                                <button type="submit" class="btn black btn-block">Request Information <span class="arrow"></span> </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!-- Schedule Showing -->
        <div class="modal fade" id="schedule" tabindex="-1" role="dialog" aria-labelledby="schedule" aria-hidden="true">
            <div class="modal-dialog custom-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Schedule Showing</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">                                        
                                        <label class="text-uppercase"><strong>First Name <span class="required_field">*</span></strong></label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="text-uppercase"><strong>Last Name <span class="required_field">*</span></strong></label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                            </div> <!-- row -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="text-uppercase"><strong>Email <span class="required_field">*</span></strong></label>
                                        <input type="email" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="text-uppercase"><strong>Phone <span class="required_field">*</span></strong></label>
                                        <input type="tel" class="form-control" required>
                                    </div>
                                </div>
                            </div> <!-- row -->
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="text-uppercase"><strong>Preferred Time and Date <span class="required_field">*</span></strong></label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select class="form-control">
                                                    <option>8:00 AM</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control">
                                                    <option>20-11-2018</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="text-uppercase"><strong>Alternate Time and Date <span class="required_field">*</span></strong></label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select class="form-control">
                                                    <option>8:00 AM</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control">
                                                    <option>20-11-2018</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- row -->
                            <div class="form-group">
                                <label class="text-uppercase"><strong>Message</strong></label>
                                <textarea class="form-control"> </textarea>
                            </div>

                            <div class="form-group font-13 text-black">
                                <p>* Your name, phone number, and email address are required so that we may contact you to schedule an appointment.</p>
                            </div>                            
                            <div class="form-group">
                                <div class="signup_note">
                                    <p> By submitting this form with your telephone number you are consenting for Keela McGraw and authorized representatives to contact you even if your name is on the Federal "Do-not-call Li</p>
                                </div>
                            </div>                            
                            <div class="mt-3">
                                <button type="submit" class="btn black">Submit <span class="arrow"></span> </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php include('includes/footer_assets.php'); ?>
    </body>
</html>

