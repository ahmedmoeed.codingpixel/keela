<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>KEELA | Blog </title>

        <?php include('includes/header_assets.php'); ?>

    </head>

    <body>
        <?php include('includes/header.php'); ?>

        <header class="header_small" style="background-image: url('assets/img/image-1.jpg')">
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h1>Blog & News</h1>
                        </div>
                    </div>
                </div>
            </div>
        </header>


        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="blog_post">
                        <figure class="space_image">
                            <img src="assets/img/spacer1.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/image-2.jpg')"></div>
                        </figure>
                        <div class="date">OCTOBER 25, 2018</div>
                        <a href="#" class="title"> <h5>How Did Arlington’s Housing Market Do In September? | How’s The Market? #11 </h5></a>
                        <div class="short_description">
                            Stylish contemporary nestled behind a private gated entrance situated on 2.5 acres and surrounded by lush landscaped grounds that offers amazing views. Home showcases an impressive media room, fitness studio, culinary kitchen with two built-in refrigerators..
                        </div>
                    </div>
                </div> <!-- col -->
                <div class="col-md-6">
                    <div class="blog_post">
                        <figure class="space_image">
                            <img src="assets/img/spacer1.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/image-3.jpg')"></div>
                        </figure>
                        <div class="date">OCTOBER 25, 2018</div>
                        <a href="#" class="title"> <h5>How Did Arlington’s Housing Market Do In September? | How’s The Market? #11 </h5></a>
                        <div class="short_description">
                            Stylish contemporary nestled behind a private gated entrance situated on 2.5 acres and surrounded by lush landscaped grounds that offers amazing views. Home showcases an impressive media room, fitness studio, culinary kitchen with two built-in refrigerators..
                        </div>
                    </div>
                </div> <!-- col -->
                <div class="col-md-6">
                    <div class="blog_post">
                        <figure class="space_image">
                            <img src="assets/img/spacer1.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/image-4.jpg')"></div>
                        </figure>
                        <div class="date">OCTOBER 25, 2018</div>
                        <a href="#" class="title"> <h5>How Did Arlington’s Housing Market Do In September? | How’s The Market? #11 </h5></a>
                        <div class="short_description">
                            Stylish contemporary nestled behind a private gated entrance situated on 2.5 acres and surrounded by lush landscaped grounds that offers amazing views. Home showcases an impressive media room, fitness studio, culinary kitchen with two built-in refrigerators..
                        </div>
                    </div>
                </div> <!-- col -->
                <div class="col-md-6">
                    <div class="blog_post">
                        <figure class="space_image">
                            <img src="assets/img/spacer1.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/image-5.jpg')"></div>
                        </figure>
                        <div class="date">OCTOBER 25, 2018</div>
                        <a href="#" class="title"> <h5>How Did Arlington’s Housing Market Do In September? | How’s The Market? #11 </h5></a>
                        <div class="short_description">
                            Stylish contemporary nestled behind a private gated entrance situated on 2.5 acres and surrounded by lush landscaped grounds that offers amazing views. Home showcases an impressive media room, fitness studio, culinary kitchen with two built-in refrigerators..
                        </div>
                    </div>
                </div> <!-- col -->
                <div class="col-md-6">
                    <div class="blog_post">
                        <figure class="space_image">
                            <img src="assets/img/spacer1.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/image-6.jpg')"></div>
                        </figure>
                        <div class="date">OCTOBER 25, 2018</div>
                        <a href="#" class="title"> <h5>How Did Arlington’s Housing Market Do In September? | How’s The Market? #11 </h5></a>
                        <div class="short_description">
                            Stylish contemporary nestled behind a private gated entrance situated on 2.5 acres and surrounded by lush landscaped grounds that offers amazing views. Home showcases an impressive media room, fitness studio, culinary kitchen with two built-in refrigerators..
                        </div>
                    </div>
                </div> <!-- col -->
                <div class="col-md-6">
                    <div class="blog_post">
                        <figure class="space_image">
                            <img src="assets/img/spacer1.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/image-7.jpg')"></div>
                        </figure>
                        <div class="date">OCTOBER 25, 2018</div>
                        <a href="#" class="title"> <h5>How Did Arlington’s Housing Market Do In September? | How’s The Market? #11 </h5></a>
                        <div class="short_description">
                            Stylish contemporary nestled behind a private gated entrance situated on 2.5 acres and surrounded by lush landscaped grounds that offers amazing views. Home showcases an impressive media room, fitness studio, culinary kitchen with two built-in refrigerators..
                        </div>
                    </div>
                </div> <!-- col -->

                <div class="col-md-12 mt-5 mb-5">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination mb-0">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&lsaquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&rsaquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div> <!-- col -->
            </div> <!-- row -->
        </div> <!-- container -->


        <?php include('includes/footer.php'); ?>
        <?php include('includes/footer_assets.php'); ?>
    </body>
</html>