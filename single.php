<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>KEELA | Blog Detail </title>

        <?php include('includes/header_assets.php'); ?>

    </head>

    <body>
        <?php include('includes/header.php'); ?>

        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="fixed_image_width"></div>
                    <div class="fixed_side_wrap">
                        <div class="fixed_image">
                            <div class="image_box" style="background-image: url('assets/img/image-9.jpg')">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="post_detail">
                        <div class="date"> <strong>OCTOBER 25, 2018 </strong></div>
                        <h1>How Did Arlington’s Housing Market Do In September? | How’s The Market? #11</h1>
                        <p>In this episode of How’s The Market?, I’m recapping the September 2018 housing market data for Arlington TX. You’ve heard so much about the entertainment growth the city is undergoing (i.e. Texas Live!, Urban Union), you may be wondering how the housing market is doing!</p>

                        <iframe src="https://www.youtube.com/embed/-yrI-E27Y-A" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <div class="transcripts">
                            <div class="head">CLICK TO HIDE VIDEO TRANSCRIPT <span></span></div>
                            <div class="body">
                                <p>Hello everyone, this is Keela McGraw with CearnalCo Realtors with a market update. This market update, we’ll cover the city of Arlington in the month of August 2018. In August of 2018, there were 503 new listings. This is actually down from this time last year, August of 2017 where there were 532 new listings actually. So as far as active listings, there were 759 active listings in August, this is up significantly from August of 2017 when there were only 650 under contract.</p>
                                <p>We had 365 homes that went under contract in August. This is down from August of 2017 when we had 426 under contract.</p>
                                <p>As far as homes that have sold from January to August of 2018, there were 2,950. This number is actually down from August 2017. Well 2017 January to August where there were 3154 homes that have sold in that time span.</p>
                                <p>The average sales price for homes in Arlington in the month of August for $229,419. In August of 2017, that number was $218,277.</p>
                                <p>Average days on market in August of 2017 was 21. In August of 2018 we’re at 22.</p>
                                <p>Last but not least, let’s look at the months of inventory. Anything between zero to four months is considered a seller’s market. Anything between four to seven months is a balanced or neutral market. And anything more than seven months is considered a buyer’s market.</p>
                                <p>In August of 2018, we’re at 2.2 months of inventory. That simply means that if there were no more homes to hit the market, in Arlington, it would take about 2.2 months to sell every home in Arlington. As you can see, if you’ve looked at past episodes, this months of inventory is creeping up slowly. So if you are even considering selling your home, this will be a great time to do that so that you can get the most out of the equity that you have built into your home.</p>
                                <p>If you do have a home that is on the market and it has been on the market for more than 22 days, there are some things that you can consider. You can always consider the location, the condition, the professional presentation, and of course the price.</p>
                                <p>The location. You can’t do a whole lot about that. The condition, of course you can try to do some upgrades or things to make it more appealing to buyers. The professional presentation. That can be something that you can discuss with your Realtor. And last but not least, if all of those things don’t work, then you can also consider a price adjustment or modification.</p>
                                <p>Again, this is Keela McGraw with CearnalCo Realtors, and I’d be happy to send you a market update or give you the value of your home to help you better generate if you’re ready to put your house on the market and tap into that wonderful equity. If you’re in the market to purchase a home, I can also help you with that. And as you can see, what kind of creeping towards a more neutral market, which means that you have a little bit more leverage when you’re placing those offers on homes. Thanks and have a great day.</p>
                            </div>
                        </div>
                        <p>I hope you enjoy this episode of How’s The Market?</p>
                        <p>Do you want information on the housing market<br/>
                            in your area? Contact Me.</p>
                        <p>Want to know what your home might sell for? Contact Me.</p>
                        <p>Are you thinking about buying a<br/>
                            home? I’d love to help! Contact Me.</p>

                        <div class="quotation">
                            Keela is great to work with. She’s polite, friendly and professional. She understands the real estate business and knowledgeable about the market. She’s highly responsive and frequently provides progress reports. I highly recommend her services. 
                            <strong>— Tica F</strong>.
                        </div>
                    </div>

                </div>
            </div> <!-- row -->
        </div> <!-- container -->


        <?php include('includes/footer.php'); ?>
        <?php include('includes/footer_assets.php'); ?>

        <script>

            $(document).ready(function () {
                function imageWidth() {
                    var img_width = $('.fixed_image_width').innerWidth();
                    $('.fixed_side_wrap .image_box').css({
                        width: img_width
                    });
                }
                imageWidth();
                $(window).resize(function () {
                    imageWidth();
                });

                $('.transcripts .head').click(function () {
                    $(this).parents('.transcripts').toggleClass('script_close');
                    $(this).next(".body").slideToggle("fast");
                    scrollSticky();
                });

                function scrollSticky() {
                    var $sticky = $('.fixed_side_wrap .image_box');
                    var $stickyrStopper = $('.site_footer');
                    if (!!$sticky.offset()) { // make sure ".sticky" element exists

                        var generalSidebarHeight = $sticky.innerHeight();
                        var stickyTop = $sticky.offset().top;

                        var stickOffset = 0;

                        var stickyStopperPosition = $stickyrStopper.offset().top;
                        console.log(stickyStopperPosition);
                        var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;

                        var diff = stopPoint + stickOffset;

                        $(window).scroll(function () { // scroll event
                            var windowTop = $(window).scrollTop(); // returns number

                            if (stopPoint < windowTop) {
                                $sticky.css({position: 'absolute', top: diff});
                                $('.fixed_side_wrap .image_box').removeClass('sticky_img');
                                $('.fixed_side_wrap .image_box').addClass('normal_img');
                            } else if (stickyTop < windowTop + stickOffset) {
                                $sticky.css({position: 'fixed', top: stickOffset});
                                $('.fixed_side_wrap .image_box').addClass('sticky_img');
                                $('.fixed_side_wrap .image_box').removeClass('normal_img');
                            } else {
                                $sticky.css({position: 'absolute', top: 'initial'});
                                $('.fixed_side_wrap .image_box').removeClass('sticky_img');
                                $('.fixed_side_wrap .image_box').addClass('normal_img');
                            }
                        });
                    }
                }
                scrollSticky();
            });

        </script>
    </body>
</html>