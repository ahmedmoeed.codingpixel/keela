<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>KEELA | Testimonials </title>

        <?php include('includes/header_assets.php'); ?>

    </head>

    <body>
        <?php include('includes/header.php'); ?>

        <header class="header_small" style="background-image: url('assets/img/image-8.jpg')">
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h1>Client Testimonials</h1>
                        </div>
                    </div>
                </div>
            </div>
        </header>




        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="testimonial">
                        <figure class="space_image">
                            <img src="assets/img/spacer1.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/testimonial-1.jpg')"></div>
                        </figure>
                        <div class="testimonial_content d-flex flex-column justify-content-center">
                            <div class="text">
                                <p>“We had a wonderful home buying experience with Keela. She is very knowledgeable of Arlington and the surrounding areas. Most of all, she was kind. When we found our home, she could tell just be our reaction that we loved it. She then worked tirelessly to help us negotiate a great price. The lender she recommended to us made the closing process seamless. We are so blessed to have found Keela, because she helped us get into the right home, at the right time.” </p>
                            </div>
                            <div class="author"> — Paul C. </div>
                        </div>
                    </div><!-- testimonial -->
                    <div class="testimonial">
                        <figure class="space_image">
                            <img src="assets/img/spacer1.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/testimonial-2.jpg')"></div>
                        </figure>
                        <div class="testimonial_content d-flex flex-column justify-content-center">
                            <div class="text">
                                <p>“Keela was always ready to show me houses I wanted to see. She introduced me to my lender and insurance guy that has made everything smooth sailing. Even though I know I am not the only one they worked with, they made me feel like I was a top priority.</p>
                            </div>
                            <div class="author"> — Liz S. </div>
                        </div>
                    </div><!-- testimonial -->
                    <div class="testimonial">
                        <figure class="space_image">
                            <img src="assets/img/spacer1.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/testimonial-3.jpg')"></div>
                        </figure>
                        <div class="testimonial_content d-flex flex-column justify-content-center">
                            <div class="text">
                                <p>“Keela is very knowledgeable. Keela respected my wishes and what I was looking for. Most importantly I felt like Keela wanted to help me find a home. She is passionate about her work and she was truly dedicated to me and my family. Keela made me feel optimistic. I had so many questions due to this being my first time purchasing a home. She always kept the communication between us open and welcoming at any time of the day. She presented the absolute perfect home for us.”</p>
                            </div>
                            <div class="author"> — Anna H. </div>
                        </div>
                    </div><!-- testimonial -->
                    <div class="testimonial">
                        <figure class="space_image">
                            <img src="assets/img/spacer1.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/testimonial-4.jpg')"></div>
                        </figure>
                        <div class="testimonial_content d-flex flex-column justify-content-center">
                            <div class="text">
                                <p>“Keela is FANTASTIC! When my search started for a home, we found Keela as a recommendation by Zillow. Keela extended the offer to meet her at her office and teach us the process for buying a house. She was always available to answer our questions via email, text, and phone call. We went looking at houses about three times. When we found the house we loved, Keela was able to request a fair offer (and didn’t jump our budget!). Our offer was on the table with other offers but Keela talked to the sellers and put us at the top of the selections. She negotiated some great perks for us and in the end, we walked away with our dream home. I am so thankful for Keela, she made buying my first house memorable. I would recommend her to anyone buying in the DFW area.” </p>
                            </div>
                            <div class="author"> — Kristina H. </div>
                        </div>
                    </div><!-- testimonial -->
                    <div class="testimonial">
                        <figure class="space_image">
                            <img src="assets/img/spacer1.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/testimonial-5.jpg')"></div>
                        </figure>
                        <div class="testimonial_content d-flex flex-column justify-content-center">
                            <div class="text">
                                <p>Keela is great to work with. She’s polite, friendly and professional. She understands the real estate business and knowledgeable about the market. She’s highly responsive and frequently provides progress reports. I highly recommend her services. </p>
                            </div>
                            <div class="author"> — Tica F. </div>
                        </div>
                    </div><!-- testimonial -->
                </div> <!-- col -->
            </div> <!-- row -->
        </div> <!-- container -->


        <?php include('includes/footer.php'); ?>
        <?php include('includes/footer_assets.php'); ?>
    </body>
</html>