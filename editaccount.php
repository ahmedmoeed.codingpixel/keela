<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>KEELA | Edit Account </title>

        <?php include('includes/header_assets.php'); ?>

    </head>

    <body>
        <?php include('includes/header.php'); ?>

        <header class="header_small" style="background-image: url('assets/img/image-16.jpg')">
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h1>Edit Account</h1>
                        </div>
                    </div>
                </div>
            </div>
        </header>


        <div class="container pb-5">
            <div class="row">
                <div class="col-md-6">                    
                    <form class="mt-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-4">
                                    <h4 class="font-weight-bold">Personal Info</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><strong>YOUR NAME</strong></label>
                                    <input type="text" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><strong>YOUR EMAIL</strong></label>
                                    <input type="email" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><strong>EMAIL</strong></label>
                                    <input type="text" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><strong>PHONE</strong></label>
                                    <input type="text" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <form class="mt-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-4">
                                    <h4 class="font-weight-bold">Address</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><strong>ADDRESS</strong></label>
                                    <input type="text" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><strong>CITY</strong></label>
                                    <input type="text" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><strong>STATE</strong></label>
                                    <input type="email" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><strong>ZIP CODW</strong></label>
                                    <input type="text" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> <!-- row -->
            <div class="row">
                <div class="col-md-6">                    
                    <form class="mt-4">
                        <div class="mb-4">
                            <h4 class="font-weight-bold">Change Password</h4>
                        </div>
                        <div class="form-group">
                            <label><strong>OLD PASSWORD</strong></label>
                            <input type="text" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label><strong>NEW PASSWORD</strong></label>
                            <input type="email" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label><strong>REPEAT NEW PASSWORD</strong></label>
                            <input type="text" class="form-control" />
                        </div>
                        <div class="note pt-5 pb-5">
                            <p><span style="color: red;">*</span> Required fields. Your personal information is strictly confidential and will not be
                                shared with any outside organizations. </p>

                            <p>By submitting this form with your telephone number you are consenting for
                            Keela McGraw and authorized representatives to contact you even if your
                            name is on the Federal "Do-not-call List." </p>
                        </div>
                        <button type="submit" class="btn black">Submit <span class="arrow"></span></button>
                    </form>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->


        <?php include('includes/footer.php'); ?>
        <?php include('includes/footer_assets.php'); ?>
    </body>
</html>