$(document).ready(function () {
    $('.menu_toggle button').on('click', function () {
        $('body').toggleClass('menu-open');

    });

    $('#owl_soon').hide();
    $('.slider_nav button.button_slide').on('click', function () {
        var target = "#" + $(this).data("target");
        console.log(target);
        $(".downtown_slider .slider_content").not(target).hide();
        $(".downtown_slider .slider_content").not(target).removeClass('d-flex');
        $(target).show();
        $(target).addClass('d-flex');
    });

    $('.slider_navigation li').click(function () {
        var current_item = $(this).attr('data-num');
        $(this).siblings('li').removeClass('active');
        $(this).addClass('active');
        
        $('.custom_slider .image').removeClass('active');
        $('.custom_slider .image[data-num='+  current_item +']').addClass('active');
        
        $('.custom_slider .content').removeClass('active');
        $('.custom_slider .content[data-num='+  current_item +']').addClass('active');
        
        console.log(current_item);
    });

});