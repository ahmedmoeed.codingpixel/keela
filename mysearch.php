<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>KEELA | My Search </title>
        <?php include('includes/header_assets.php'); ?>
    </head>

    <body>
        <?php include('includes/header.php'); ?>

        <header class="header_small" style="background-image: url('assets/img/image-17.jpg')">
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h1>My Searches</h1>
                        </div>
                    </div>
                </div>
            </div>
        </header>


        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="saved_search">
                        <div class="head">
                            <div class="date"><strong>Date: </strong> 18-11-2018</div>
                            <h4>212 Search Matches</h4>
                        </div>
                        <div class="detail">
                            <div class="d-flex">
                                <div class="left-side">CITIES</div>
                                <div class="right-side">Arlington, Grand Prairie, Mansfield</div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">PROPERTY TYPE</div>
                                <div class="right-side">Single Family Home</div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">MIN. PRICE</div>
                                <div class="right-side">$200,000 </div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">MAX. PRICE</div>
                                <div class="right-side">$300,000 </div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">BEDS</div>
                                <div class="right-side">3</div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">BATHS</div>
                                <div class="right-side">2</div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">SQ.FT</div>
                                <div class="right-side">500+</div>
                            </div>
                        </div>
                        <div class="footer">
                            <div class="updates d-flex align-items-center">
                                <span class="font-weight-semibold mr-4">EMAIL UPDATES</span>
                                <div class="email_updates d-flex align-items-center">
                                    <span>No</span>
                                    <div class="custom_checkbox mx-2">
                                        <input type="checkbox">
                                        <div class="check_box d-flex align-items-center"><div></div></div>
                                    </div>
                                    <span>Yes</span>
                                </div>
                            </div>
                            <div class="btns">
                                <div class="actions_btn d-flex">
                                    <a href="#" class="edit_btn btn">Edit</a>
                                    <a href="#" class="delete_btn btn">Delete</a>
                                </div>
                                <a href="#" class="read_more black d-flex align-items-center justify-content-center">
                                    <i class="icon-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> <!-- col -->
                <div class="col-md-6">
                    <div class="saved_search">
                        <div class="head">
                            <div class="date"><strong>Date: </strong> 18-11-2018</div>
                            <h4>212 Search Matches</h4>
                        </div>
                        <div class="detail">
                            <div class="d-flex">
                                <div class="left-side">CITIES</div>
                                <div class="right-side">Arlington, Grand Prairie, Mansfield</div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">PROPERTY TYPE</div>
                                <div class="right-side">Single Family Home</div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">MIN. PRICE</div>
                                <div class="right-side">$200,000 </div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">MAX. PRICE</div>
                                <div class="right-side">$300,000 </div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">BEDS</div>
                                <div class="right-side">3</div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">BATHS</div>
                                <div class="right-side">2</div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">SQ.FT</div>
                                <div class="right-side">500+</div>
                            </div>
                        </div>
                        <div class="footer">
                            <div class="updates d-flex align-items-center">
                                <span class="font-weight-semibold mr-4">EMAIL UPDATES</span>
                                <div class="email_updates d-flex align-items-center">
                                    <span>No</span>
                                    <div class="custom_checkbox mx-2">
                                        <input type="checkbox">
                                        <div class="check_box d-flex align-items-center"><div></div></div>
                                    </div>
                                    <span>Yes</span>
                                </div>
                            </div>
                            <div class="btns">
                                <div class="actions_btn d-flex">
                                    <a href="#" class="edit_btn btn">Edit</a>
                                    <a href="#" class="delete_btn btn">Delete</a>
                                </div>
                                <a href="#" class="read_more black d-flex align-items-center justify-content-center">
                                    <i class="icon-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> <!-- col -->
                <div class="col-md-6">
                    <div class="saved_search">
                        <div class="head">
                            <div class="date"><strong>Date: </strong> 18-11-2018</div>
                            <h4>212 Search Matches</h4>
                        </div>
                        <div class="detail">
                            <div class="d-flex">
                                <div class="left-side">CITIES</div>
                                <div class="right-side">Arlington, Grand Prairie, Mansfield</div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">PROPERTY TYPE</div>
                                <div class="right-side">Single Family Home</div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">MIN. PRICE</div>
                                <div class="right-side">$200,000 </div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">MAX. PRICE</div>
                                <div class="right-side">$300,000 </div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">BEDS</div>
                                <div class="right-side">3</div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">BATHS</div>
                                <div class="right-side">2</div>
                            </div>
                            <div class="d-flex">
                                <div class="left-side">SQ.FT</div>
                                <div class="right-side">500+</div>
                            </div>
                        </div>
                        <div class="footer">
                            <div class="updates d-flex align-items-center">
                                <span class="font-weight-semibold mr-4">EMAIL UPDATES</span>
                                <div class="email_updates d-flex align-items-center">
                                    <span>No</span>
                                    <div class="custom_checkbox mx-2">
                                        <input type="checkbox">
                                        <div class="check_box d-flex align-items-center"><div></div></div>
                                    </div>
                                    <span>Yes</span>
                                </div>
                            </div>
                            <div class="btns">
                                <div class="actions_btn d-flex">
                                    <a href="#" class="edit_btn btn">Edit</a>
                                    <a href="#" class="delete_btn btn">Delete</a>
                                </div>
                                <a href="#" class="read_more black d-flex align-items-center justify-content-center">
                                    <i class="icon-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> <!-- col -->
            </div> <!-- row -->
        </div> <!-- container -->


        <?php include('includes/footer.php'); ?>
        <?php include('includes/footer_assets.php'); ?>
    </body>
</html>