<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>KEELA | Favourite </title>

        <?php include('includes/header_assets.php'); ?>

    </head>

    <body>
        <?php include('includes/header.php'); ?>

        <header class="header_small" style="background-image: url('assets/img/image-10.jpg')">
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h1>My Favorites</h1>
                        </div>
                    </div>
                </div>
            </div>
        </header>


        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="favourite_product">
                        <figure class="space_image">
                            <img src="assets/img/spacer2.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/image-11.jpg')"></div>
                        </figure>
                        <div class="align-items-end d-flex overlay_content">
                            <div class="inner_container">
                                <h3 class="title">4685 Old Pond DrivePlano </h3>
                                <div class="price"> $469,000</div>
                                <ul class="features">
                                    <li>3,218 SQFT</li>
                                    <li> 4 BEDROOMS</li>
                                    <li>3.5 BATHROOMS</li>
                                </ul>
                                <div class="favourite_btns">
                                    <a href="#" class="fav_remove"> <i class="fas fa-heart"></i> Remove Favorite</a>
                                    <a href="#" class="undo_remove"> <i class="fas fa-undo"></i> Undo Remove</a>
                                </div>
                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                    <i class="icon-right"></i>
                                </a>
                            </div>
                        </div>
                    </div> <!-- favourite -->
                </div> <!-- col -->
                <div class="col-lg-4 col-md-6">
                    <div class="favourite_product">
                        <figure class="space_image">
                            <img src="assets/img/spacer2.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/image-12.jpg')"></div>
                        </figure>
                        <div class="align-items-end d-flex overlay_content">
                            <div class="inner_container">
                                <h3 class="title">4685 Old Pond DrivePlano </h3>
                                <div class="price"> $469,000</div>
                                <ul class="features">
                                    <li>3,218 SQFT</li>
                                    <li> 4 BEDROOMS</li>
                                    <li>3.5 BATHROOMS</li>
                                </ul>
                                <div class="favourite_btns">
                                    <a href="#" class="fav_remove"> <i class="fas fa-heart"></i> Remove Favorite</a>
                                </div>
                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                    <i class="icon-right"></i>
                                </a>
                            </div>
                        </div>
                    </div> <!-- favourite -->
                </div> <!-- col -->
                <div class="col-lg-4 col-md-6">
                    <div class="favourite_product">
                        <figure class="space_image">
                            <img src="assets/img/spacer2.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/image-13.jpg')"></div>
                        </figure>
                        <div class="align-items-end d-flex overlay_content">
                            <div class="inner_container">
                                <h3 class="title">4685 Old Pond DrivePlano </h3>
                                <div class="price"> $469,000</div>
                                <ul class="features">
                                    <li>3,218 SQFT</li>
                                    <li> 4 BEDROOMS</li>
                                    <li>3.5 BATHROOMS</li>
                                </ul>
                                <div class="favourite_btns">
                                    <a href="#" class="fav_remove"> <i class="fas fa-heart"></i> Remove Favorite</a>
                                </div>
                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                    <i class="icon-right"></i>
                                </a>
                            </div>
                        </div>
                    </div> <!-- favourite -->
                </div> <!-- col -->
                <div class="col-lg-4 col-md-6">
                    <div class="favourite_product">
                        <figure class="space_image">
                            <img src="assets/img/spacer2.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/image-14.jpg')"></div>
                        </figure>
                        <div class="align-items-end d-flex overlay_content">
                            <div class="inner_container">
                                <h3 class="title">4685 Old Pond DrivePlano </h3>
                                <div class="price"> $469,000</div>
                                <ul class="features">
                                    <li>3,218 SQFT</li>
                                    <li> 4 BEDROOMS</li>
                                    <li>3.5 BATHROOMS</li>
                                </ul>
                                <div class="favourite_btns">
                                    <a href="#" class="fav_remove"> <i class="fas fa-heart"></i> Remove Favorite</a>
                                </div>
                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                    <i class="icon-right"></i>
                                </a>
                            </div>
                        </div>
                    </div> <!-- favourite -->
                </div> <!-- col -->
                <div class="col-lg-4 col-md-6">
                    <div class="favourite_product">
                        <figure class="space_image">
                            <img src="assets/img/spacer2.png" alt="" />
                            <div class="image" style="background-image: url('assets/img/image-15.jpg')"></div>
                        </figure>
                        <div class="align-items-end d-flex overlay_content">
                            <div class="inner_container">
                                <h3 class="title">4685 Old Pond DrivePlano </h3>
                                <div class="price"> $469,000</div>
                                <ul class="features">
                                    <li>3,218 SQFT</li>
                                    <li> 4 BEDROOMS</li>
                                    <li>3.5 BATHROOMS</li>
                                </ul>
                                <div class="favourite_btns">
                                    <a href="#" class="fav_remove"> <i class="fas fa-heart"></i> Remove Favorite</a>
                                </div>
                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                    <i class="icon-right"></i>
                                </a>
                            </div>
                        </div>
                    </div> <!-- favourite -->
                </div> <!-- col -->
            </div> <!-- row -->
        </div> <!-- container -->


        <?php include('includes/footer.php'); ?>
        <?php include('includes/footer_assets.php'); ?>
    </body>
</html>