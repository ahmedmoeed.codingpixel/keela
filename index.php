<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>KEELA | Index </title>
        <?php include('includes/header_assets.php'); ?>
    </head>

    <body>
        <?php include('includes/header.php'); ?>
        <!-- Banner -->
        <div class="align-items-center d-flex home-banner jumbotron" style="background-image:url('assets/img/banner.jpg')">
            <div class="container">
                <div class="banner_content">
                    <div class="title">
                        <h1>Engage with Keela and Her Team</h1>
                    </div>
                    <p class="lead">Get the best home buying and selling experience.</p>
                    <div class="btns d-flex">
                        <a class="btn_white d-flex align-items-center" href="#">Listing<span class="icon-right"></span></a>
                        <a class="btn_transparent" href="#">Contact Keela Today</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="about_home_section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="d-flex inner_wrap align-items-lg-center flex-column flex-lg-row">
                            <div>
                                <div class="user_img" style="background-image: url('assets/img/image-21.jpg');">
                                </div>
                            </div>
                            <div class="text_content">
                                <p>As an Arlington resident and former educator, I am a committed REALTOR® helping clients buy
                                    and sell new and existing homes in Arlington and surrounding communities. With extensive
                                    experience in education as a teacher of hearing-impaired students, then as a campus
                                    administrator of a K-6 school and finally as an educational consultant, I bring a unique
                                    perspective.Leveraging my experience as a successful educator, I work hard to provide
                                    outstanding client service and open communication to ensure my client’s best interests are
                                    at the forefront of every decision. I firmly believe that it is my duty to represent my
                                    clients to the best of my ability. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="counter_section" style="background-image: url('assets/img/image-18.jpg');">
            <div class="overlay">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h2>Over 1 billion dollars sold.</h2>
                            <div class="text_content">
                                <p>I am a passionate REALTOR® with CearnalCo, Realtors® and the founder of ArlingtonTexasToday.com, a digital lifestyle guide for Arlington residents. </p>
                            </div>

                            <ul class="counter_wrap d-flex flex-wrap justify-content-center">
                                <li>
                                    <div class="counter_box">
                                        <span class="number">86</span>
                                        <p>Walk Score</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="counter_box">
                                        <span class="number">89</span>
                                        <p>Transit Score</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="counter_box">
                                        <span class="number">98</span>
                                        <p>Biker’s Paradise</p>
                                    </div>
                                </li>
                            </ul>
                        </div> <!-- col -->
                    </div> <!-- row -->
                </div> <!-- container -->
            </div> <!-- overlay -->
        </div>

        <div class="image_content_section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-6">
                        <div class="image">
                            <img src="assets/img/img1.jpg" alt="" class="img-fluid"/>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="text_content">
                            <p>In addition to real estate facts and figures, I feel it is valuable for clients to have
                                lifestyle information about their town. That’s why I co-founded ArlingtonTexasToday.com.
                                Arlington Texas Today is a digital lifestyle guide helping Arlington residents find fun
                                things to do and the best places to eat & live around town.</p>
                            <p>The free resource publishes articles Monday – Friday and is read by over 40,000 people each
                                month.</p>
                        </div>
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div>

        <div class="image_content_section">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-12 col-md-6">
                        <div class="image">
                            <img src="assets/img/img2.jpg" alt="" class="img-fluid"/>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="text_content">
                            <p>As an Arlington resident and former educator, I am a committed REALTOR® helping clients buy and sell new and existing homes in Arlington and surrounding communities. With extensive
                                experience in education as a teacher of hearing-impaired students, then as a campus administrator of a K-6 school and finally as an educational consultant, I bring a unique perspective.</p>
                            <p>We don’t put geographical limits on our services, our local knowledge is supported by Keela and her team – if we’re not on the ground somewhere, they are.</p>
                        </div>
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div>

        <div class="featured_listings_section" style="background-image: url('assets/img/image-19.jpg');">
            <div class="overlay">
                <div class="container">
                    <div class="row align-items-center flex-row-reverse">
                        <div class="col-md-8 d-flex flex-column flex-sm-row">
                            <div class="favourite_product">
                                <figure class="space_image">
                                    <img src="assets/img/spacer2.png" alt="" />
                                    <div class="image" style="background-image: url('assets/img/image-11.jpg')"></div>
                                </figure>
                                <div class="align-items-end d-flex overlay_content">
                                    <div class="inner_container">
                                        <h4 class="title">4685 Old Pond DrivePlano </h4>
                                        <div class="price"> $469,000</div>
                                        <ul class="features">
                                            <li>3,218 SQFT</li>
                                            <li> 4 BEDROOMS</li>
                                            <li>3.5 BATHROOMS</li>
                                        </ul>
                                        <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                            <i class="icon-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div> <!-- favourite -->
                            <div class="favourite_product">
                                <figure class="space_image">
                                    <img src="assets/img/spacer2.png" alt="" />
                                    <div class="image" style="background-image: url('assets/img/image-11.jpg')"></div>
                                </figure>
                                <div class="align-items-end d-flex overlay_content">
                                    <div class="inner_container">
                                        <h4 class="title">4685 Old Pond DrivePlano </h4>
                                        <div class="price"> $469,000</div>
                                        <ul class="features">
                                            <li>3,218 SQFT</li>
                                            <li> 4 BEDROOMS</li>
                                            <li>3.5 BATHROOMS</li>
                                        </ul>
                                        <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                            <i class="icon-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div> <!-- favourite -->
                        </div>
                        <div class="col-md-4">
                            <div class="featured_box">
                                <h2>FEATURED <br />LISTINGS</h2>
                                <p>Your Realtor® for Life!</p>
                                <a href="#" class="btn white">See All <i class="arrow"></i></a>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div> <!-- overlay -->
            </div> <!-- container -->
        </div>

        <div class="neighbour_section">
            <div class="container">
                <div class="row flex-row-reverse">
                    <div class="col-12 col-sm-12 col-md-7">
                        <div class="custom_slider">
                            <figure class="space_image">
                                <img src="assets/img/spacer3.png" alt="">
                                <div class="image active" data-num="1" style="background-image: url('assets/img/image-20.jpg')"></div>
                                <div class="image" data-num="2" style="background-image: url('assets/img/img1.jpg')"></div>
                                <div class="image" data-num="3" style="background-image: url('assets/img/img2.jpg')"></div>
                            </figure>
                            <ul class="slider_navigation">
                                <li class="active" data-num="1">
                                    <span class="text"> Viridian</span>
                                    <span class="number"> 1 </span>
                                </li>
                                <li data-num="2">
                                    <span class="text"> Viridian </span>
                                    <span class="number"> 2 </span>
                                </li>
                                <li data-num="3">
                                    <span class="text"> Viridian </span>
                                    <span class="number">3 </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-5">
                        <div class="slider_content_wrap">
                            <div class="custom_slider">
                                <div class="content active" data-num="1">
                                    <h3>Experience the sights of Downtown</h3>
                                    <p>Tour KEELA’S Neighberhoods</p>
                                    <a href="#" class="btn black">Start Exploring <i class="arrow"></i></a>
                                </div>
                                <div class="content" data-num="2">
                                    <h3>Experience the sights of Downtown Home</h3>
                                    <p>Tour KEELA’S Homes </p>
                                    <a href="#" class="btn black">Start Exploring <i class="arrow"></i></a>
                                </div>
                                <div class="content" data-num="3">
                                    <h3>Experience the sights of Downtown Hotel</h3>
                                    <p>Tour KEELA’S Hotel</p>
                                    <a href="#" class="btn black">Start Exploring <i class="arrow"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->
            </div> <!-- container -->
        </div>

        <?php include('includes/footer.php'); ?>
        <?php include('includes/footer_assets.php'); ?>
    </body>
</html>