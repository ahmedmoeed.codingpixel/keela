<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>KEELA | Search Result </title>
        <?php include('includes/header_assets.php'); ?>
    </head>

    <body>
        <?php include('includes/header.php'); ?>

        <div class="search_result_page">
            <div class="container-fluid">
                <div class="search_header d-flex">
                    <div class="btns d-flex ml-auto">
                        <a href="#" class="btn">Save Search</a>
                        <a href="#" class="btn black">Advanced Property Filter</a>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="search_map">
                            <div id="map"></div>
                        </div>
                    </div> <!-- col -->
                    <div class="col-md-6">
                        <div class="search_result_head">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="result_found">Showing <strong>15</strong> of <strong>113</strong> results</div>
                                </div>
                            </div>
                        </div>
                        <div class="search_listing">
                            <ul class="d-flex flex-wrap">
                                <li>
                                    <div class="favourite_product">
                                        <figure class="space_image">
                                            <img src="assets/img/spacer2.png" alt="" />
                                            <div class="image" style="background-image: url('assets/img/image-11.jpg')"></div>
                                        </figure>
                                        <div class="align-items-end d-flex overlay_content">
                                            <div class="inner_container">
                                                <h4 class="title">4685 Old Pond DrivePlano </h4>
                                                <div class="price"> $469,000</div>
                                                <ul class="features">
                                                    <li>3,218 SQFT</li>
                                                    <li> 4 BEDROOMS</li>
                                                    <li>3.5 BATHROOMS</li>
                                                </ul>
                                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                                    <i class="icon-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div> <!-- favourite -->
                                </li>
                                <li>
                                    <div class="favourite_product">
                                        <figure class="space_image">
                                            <img src="assets/img/spacer2.png" alt="" />
                                            <div class="image" style="background-image: url('assets/img/image-11.jpg')"></div>
                                        </figure>
                                        <div class="align-items-end d-flex overlay_content">
                                            <div class="inner_container">
                                                <h4 class="title">4685 Old Pond DrivePlano </h4>
                                                <div class="price"> $469,000</div>
                                                <ul class="features">
                                                    <li>3,218 SQFT</li>
                                                    <li> 4 BEDROOMS</li>
                                                    <li>3.5 BATHROOMS</li>
                                                </ul>
                                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                                    <i class="icon-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div> <!-- favourite -->
                                </li>
                                <li>
                                    <div class="favourite_product">
                                        <figure class="space_image">
                                            <img src="assets/img/spacer2.png" alt="" />
                                            <div class="image" style="background-image: url('assets/img/image-11.jpg')"></div>
                                        </figure>
                                        <div class="align-items-end d-flex overlay_content">
                                            <div class="inner_container">
                                                <h4 class="title">4685 Old Pond DrivePlano </h4>
                                                <div class="price"> $469,000</div>
                                                <ul class="features">
                                                    <li>3,218 SQFT</li>
                                                    <li> 4 BEDROOMS</li>
                                                    <li>3.5 BATHROOMS</li>
                                                </ul>
                                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                                    <i class="icon-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div> <!-- favourite -->
                                </li>
                                <li>
                                    <div class="favourite_product">
                                        <figure class="space_image">
                                            <img src="assets/img/spacer2.png" alt="" />
                                            <div class="image" style="background-image: url('assets/img/image-11.jpg')"></div>
                                        </figure>
                                        <div class="align-items-end d-flex overlay_content">
                                            <div class="inner_container">
                                                <h4 class="title">4685 Old Pond DrivePlano </h4>
                                                <div class="price"> $469,000</div>
                                                <ul class="features">
                                                    <li>3,218 SQFT</li>
                                                    <li> 4 BEDROOMS</li>
                                                    <li>3.5 BATHROOMS</li>
                                                </ul>
                                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                                    <i class="icon-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div> <!-- favourite -->
                                </li>
                                <li>
                                    <div class="favourite_product">
                                        <figure class="space_image">
                                            <img src="assets/img/spacer2.png" alt="" />
                                            <div class="image" style="background-image: url('assets/img/image-11.jpg')"></div>
                                        </figure>
                                        <div class="align-items-end d-flex overlay_content">
                                            <div class="inner_container">
                                                <h4 class="title">4685 Old Pond DrivePlano </h4>
                                                <div class="price"> $469,000</div>
                                                <ul class="features">
                                                    <li>3,218 SQFT</li>
                                                    <li> 4 BEDROOMS</li>
                                                    <li>3.5 BATHROOMS</li>
                                                </ul>
                                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                                    <i class="icon-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div> <!-- favourite -->
                                </li>
                                <li>
                                    <div class="favourite_product">
                                        <figure class="space_image">
                                            <img src="assets/img/spacer2.png" alt="" />
                                            <div class="image" style="background-image: url('assets/img/image-11.jpg')"></div>
                                        </figure>
                                        <div class="align-items-end d-flex overlay_content">
                                            <div class="inner_container">
                                                <h4 class="title">4685 Old Pond DrivePlano </h4>
                                                <div class="price"> $469,000</div>
                                                <ul class="features">
                                                    <li>3,218 SQFT</li>
                                                    <li> 4 BEDROOMS</li>
                                                    <li>3.5 BATHROOMS</li>
                                                </ul>
                                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                                    <i class="icon-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div> <!-- favourite -->
                                </li>
                                <li>
                                    <div class="favourite_product">
                                        <figure class="space_image">
                                            <img src="assets/img/spacer2.png" alt="" />
                                            <div class="image" style="background-image: url('assets/img/image-11.jpg')"></div>
                                        </figure>
                                        <div class="align-items-end d-flex overlay_content">
                                            <div class="inner_container">
                                                <h4 class="title">4685 Old Pond DrivePlano </h4>
                                                <div class="price"> $469,000</div>
                                                <ul class="features">
                                                    <li>3,218 SQFT</li>
                                                    <li> 4 BEDROOMS</li>
                                                    <li>3.5 BATHROOMS</li>
                                                </ul>
                                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                                    <i class="icon-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div> <!-- favourite -->
                                </li>
                                <li>
                                    <div class="favourite_product">
                                        <figure class="space_image">
                                            <img src="assets/img/spacer2.png" alt="" />
                                            <div class="image" style="background-image: url('assets/img/image-11.jpg')"></div>
                                        </figure>
                                        <div class="align-items-end d-flex overlay_content">
                                            <div class="inner_container">
                                                <h4 class="title">4685 Old Pond DrivePlano </h4>
                                                <div class="price"> $469,000</div>
                                                <ul class="features">
                                                    <li>3,218 SQFT</li>
                                                    <li> 4 BEDROOMS</li>
                                                    <li>3.5 BATHROOMS</li>
                                                </ul>
                                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                                    <i class="icon-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div> <!-- favourite -->
                                </li>
                                <li>
                                    <div class="favourite_product">
                                        <figure class="space_image">
                                            <img src="assets/img/spacer2.png" alt="" />
                                            <div class="image" style="background-image: url('assets/img/image-11.jpg')"></div>
                                        </figure>
                                        <div class="align-items-end d-flex overlay_content">
                                            <div class="inner_container">
                                                <h4 class="title">4685 Old Pond DrivePlano </h4>
                                                <div class="price"> $469,000</div>
                                                <ul class="features">
                                                    <li>3,218 SQFT</li>
                                                    <li> 4 BEDROOMS</li>
                                                    <li>3.5 BATHROOMS</li>
                                                </ul>
                                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                                    <i class="icon-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div> <!-- favourite -->
                                </li>
                                <li>
                                    <div class="favourite_product">
                                        <figure class="space_image">
                                            <img src="assets/img/spacer2.png" alt="" />
                                            <div class="image" style="background-image: url('assets/img/image-11.jpg')"></div>
                                        </figure>
                                        <div class="align-items-end d-flex overlay_content">
                                            <div class="inner_container">
                                                <h4 class="title">4685 Old Pond DrivePlano </h4>
                                                <div class="price"> $469,000</div>
                                                <ul class="features">
                                                    <li>3,218 SQFT</li>
                                                    <li> 4 BEDROOMS</li>
                                                    <li>3.5 BATHROOMS</li>
                                                </ul>
                                                <a href="#" class="read_more d-flex align-items-center justify-content-center">
                                                    <i class="icon-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div> <!-- favourite -->
                                </li>
                            </ul>
                        </div>
                    </div> <!-- col -->
                </div> <!-- row -->
            </div> <!-- container -->
        </div>

        <?php include('includes/footer_assets.php'); ?>
        <script>

            function search_height() {
                var search_head_height = $('.search_result_head').innerHeight();
                var search_head_offset = $('.search_result_head').offset().top;
                var height = (search_head_height + search_head_offset);

                var search_height = ($(window).innerHeight() - height);
                $('.search_listing > ul').height(search_height);
            }
            search_height();
            window.addEventListener('resize', search_height);

            function initMap() {
                // Styles a map in night mode.
                var map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: 40.674, lng: -73.945},
                    zoom: 12,

                    zoomControl: false,
                    mapTypeControl: false,
                    scaleControl: false,
                    streetViewControl: false,
                    rotateControl: false,
                    fullscreenControl: false,

                    styles: [
                        {
                            "featureType": "administrative",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#444444"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#f2f2f2"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "saturation": -100
                                },
                                {
                                    "lightness": 45
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#46bcec"
                                },
                                {
                                    "visibility": "on"
                                }
                            ]
                        }
                    ]
                });
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApyIRH_zIWZT32AXvIU2A2Y-A0fvPSv50&callback=initMap"
        async defer></script>
    </body>
</html>