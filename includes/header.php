
<div class="overlay_menu"></div>
<header class="header">
    <aside class="menu_sidebar">
        <div class="menu_sticky">
            <div class="menu_toggle">
                <button class="navbar-toggler" type="button">
                    <span class="top_bar"></span>
                    <span class="middle_bar"></span>
                    <span class="bottom_bar"></span>
                </button>
            </div>
            <div class="caption"><span class="inner">Bespoke Townhomes with Keela Mcgraw</span></div>
            <div class="user_img">
                <figure style="background-image: url('assets/img/image-21.jpg')"></figure>
            </div>
        </div>
        <!-- Navbar -->

        <div class="menu_wrap_outer after-login">
            <div class="menu_wrap_inner">
                <div class="container d-flex flex-column h-100 justify-content-between">
                    <div>
                        <div class="user d-flex align-items-center">
                            <figure style="background-image: url('assets/img/image-21.jpg')"></figure>
                            <div class="user-name">
                                <h5>Kella Mcgraw</h5>
                                <span>Realtor</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <nav class="main-navbar">
                                    <ul>
                                        <li><a href="#">Meet Keela</a></li>
                                        <li><a href="#">Blog</a></li>
                                        <li><a href="#">Property search</a></li>
                                        <li><a href="#">Testimonials</a></li>
                                        <li><a href="#">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-12 col-sm-6">
                                <h6 class="mb-3"><strong>Account</strong></h6>
                                <nav class="main-navbar">
                                    <ul>
                                        <li><a href="#">My Listings</a></li>
                                        <li><a href="#">My searches</a></li>
                                        <li><a href="#">Edit Account</a></li>
                                        <li><a href="#">Logout</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="bottom_section">
                        <a href="#" class="btn white login_btn">Login <span class="arrow"></span></a>
                        <div class="signup_note">
                            <p>Do you want to <a href="#">Sign Up</a> for new account or have you <a href="#">forgotten your password?</a></p>
                        </div>
                        <hr/>
                        <div class="copyright_header">
                            <p>Copyright © 2018 · Keela McGraw, REALTOR® · <span>CearnalCo, Realtors·</span><br/>
                                500 E Front St. #120, Arlington, TX 76010 · 817-543-0000· <br/> 
                                <span>Information About Brokerage Services</span></p>
                        </div>
                    </div>
                </div>
                <div class="menu_social_links d-flex align-items-end">
                    <ul>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </aside>
</header>
