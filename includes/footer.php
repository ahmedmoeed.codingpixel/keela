<footer class="site_footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-8 col-lg-9 col-md-12">
                <div class="left-side">
                    <div class="row align-items-center flex-row-reverse">
                        <div class="col-xl-6 col-lg-5 col-md-5">
                            <div class="footer_tabs">
                                <ul class="nav nav-tabs justify-content-end" id="myTab" role="tablist">
                                    <li>
                                        <a class="active" data-toggle="tab" href="#most_viewed" role="tab" aria-selected="true">Most Viewed </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#latest_video" role="tab" aria-selected="false">Latest Video</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="most_viewed" role="tabpanel">
                                        <div class="favourite_product">
                                            <figure class="space_image">
                                                <img src="assets/img/spacer2.png" alt="" />
                                                <div class="image" style="background-image: url('assets/img/image-22.jpg')"></div>
                                            </figure>
                                            <div class="align-items-center d-flex overlay_content">
                                                <div class="inner_container">
                                                    <h5 class="title text-center">How Did Arlington's Housing Market Do In September? </h5>
                                                    <span class="icon-play"></span>
                                                </div>
                                            </div>
                                        </div> <!-- favourite -->
                                    </div>
                                    <div class="tab-pane fade" id="latest_video" role="tabpanel">
                                        <div class="favourite_product">
                                            <figure class="space_image">
                                                <img src="assets/img/spacer2.png" alt="" />
                                                <div class="image" style="background-image: url('assets/img/image-12.jpg')"></div>
                                            </figure>
                                            <div class="align-items-center d-flex overlay_content">
                                                <div class="inner_container">
                                                    <h5 class="title text-center">How Did Arlington's Housing Market Do In September? </h5>
                                                    <span class="icon-play"></span>
                                                </div>
                                            </div>
                                        </div> <!-- favourite -->
                                    </div>
                                </div>
                            </div> <!-- footer tabs -->
                        </div> <!-- col -->
                        <div class="col-xl-6 col-lg-7 col-md-7">
                            <div class="look_more">
                                <h5 class="title_bordered">LOOK MORE</h5>
                                <a href="#" class="btn white">My Projects</a>
                                <a href="#" class="btn black">Interested to Sell?</a>
                            </div>
                        </div> <!-- col -->
                    </div> <!-- row -->
                    <div class="row align-items-end">
                        <div class="col-md-8">
                            <ul class="social_footer d-flex">
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            </ul>
                            <div class="copyright">
                                <p>Copyright © 2018 · Keela McGraw, REALTOR® · CearnalCo, Realtors· <br/> 
                                    500 E Front St. #120, Arlington, TX 76010 · 817-543-0000· <br/> 
                                    Information About Brokerage Services</p>
                            </div>
                        </div> <!-- col -->
                        <div class="col-md-4">
                            <div class="developedBy">
                                Made with love at <a href="http://codingpixel.com/" target="_blank">CodingPixel</a>
                            </div>
                        </div> <!-- col -->
                    </div> <!-- row -->
                </div> <!-- left side -->
            </div> <!-- col -->
            <div class="col-xl-4 col-lg-3 col-md-12 right-side align-items-center d-flex">
                <div class="send_request">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-6 col-6">
                            <h5 class="title_bordered">OR</h5>
                            <a href="#" class="btn white">SEND REQUEST</a>
                        </div> <!-- col -->
                        <div class="col-xl-12 col-lg-12 col-md-6 col-6">
                            <h6>YOUR REALTOR® FOR LIFE</h6>                
                            <ul class="contact_detail">
                                <li>
                                    <span>T:</span>
                                    (901) 277-5839 (c)<br>
                                    (901) 756-8900 (o)
                                </li>
                                <li>
                                    <span>@:</span>
                                    keela@keelamcgraw.com
                                </li>
                            </ul>
                        </div> <!-- col -->
                    </div> <!-- row -->
                </div>
            </div> <!-- col -->
        </div> <!-- row -->
    </div> <!-- container -->
</footer> <!-- footer -->
