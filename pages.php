<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>KEELA | Testimonials </title>

        <?php include('includes/header_assets.php'); ?>

    </head>

    <body>
        <?php include('includes/header.php'); ?>

        <div class="container py-5">
            <a href="index.php" target="_blank"> index </a> <br/>
            <a href="blog.php" target="_blank"> blog </a> <br/>
            <a href="contact.php" target="_blank"> contact </a> <br/>
            <a href="editaccount.php" target="_blank"> editaccount </a> <br/>
            <a href="myfavorite.php" target="_blank"> myfavorite </a> <br/>
            <a href="mysearch.php" target="_blank"> mysearch </a> <br/>
            <a href="popup.php" target="_blank"> popup </a> <br/>
            <a href="search.php" target="_blank"> search </a> <br/>
            <a href="single.php" target="_blank"> single </a> <br/>
            <a href="testimonial.php" target="_blank"> testimonial </a> <br/>
        </div>

        <?php include('includes/footer.php'); ?>
        <?php include('includes/footer_assets.php'); ?>
    </body>
</html>